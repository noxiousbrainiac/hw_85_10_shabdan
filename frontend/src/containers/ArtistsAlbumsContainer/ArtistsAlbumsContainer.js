import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {clearAlbumsError, getAlbums} from "../../store/actions/albumsActions";
import {CircularProgress} from "@material-ui/core";
import AlbumsList from "../../components/Lists/AlbumsList/AlbumsList";

const ArtistsAlbumsContainer = ({location}) => {
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albumsReducer.albums);
    const loading = useSelector(state => state.albumsReducer.loading);

    useEffect(() => {
        dispatch(getAlbums(location.search));
        return () => {
            dispatch(clearAlbumsError());
        }
    }, [dispatch, location.search]);

    let AlbumsContainer = (
        <>
            <h2 style={{textAlign: "center"}}>Albums</h2>
            <h2>Artist: {albums[0]?.artist.title}</h2>
            <AlbumsList albums={albums}/>
        </>
    )

    if (loading === true) {
        AlbumsContainer = (
            <div
                style={{
                    position: "relative",
                    paddingLeft: "50%",
                    paddingTop: "30%"
                }}
            >
                <CircularProgress
                    color="secondary"
                    size={100}
                />
            </div>
        );
    }
    return AlbumsContainer;
};

export default ArtistsAlbumsContainer;