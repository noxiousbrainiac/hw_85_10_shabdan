import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getAlbums} from "../../store/actions/albumsActions";
import {getArtists} from "../../store/actions/artistActions";
import {Typography} from "@material-ui/core";
import TrackForm from "../../components/Forms/TrackForm/TrackForm";
import {clearTrackError, postTrack} from "../../store/actions/tracksActions";

const NewTrack = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artistReducer.artists);
    const error = useSelector(state => state.tracksReducer.postError);
    const albums = useSelector(state => state.albumsReducer.albums);

    const onSubmit = async albumData => {
        await dispatch(postTrack(albumData));
    };

    useEffect(() => {
        dispatch(getArtists());
        dispatch(getAlbums());
        return () => {
            dispatch(clearTrackError());
        }
    }, [dispatch])

    return (
        <>
            <Typography variant="h4">New track</Typography>
            <TrackForm
                onSubmit={onSubmit}
                error={error}
                artists={artists}
                albums={albums}
            />
        </>
    );
};

export default NewTrack;