import React, {useEffect} from 'react';
import {CircularProgress, Grid} from "@material-ui/core";
import ArtistsList from "../../components/Lists/ArtistsList/ArtistsList";
import {useDispatch, useSelector} from "react-redux";
import {clearArtistsError, getArtists} from "../../store/actions/artistActions";

const HomePage = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artistReducer.artists);
    const loading = useSelector(state => state.artistReducer.loading);

    useEffect(()=>{
        dispatch(getArtists());
        return () => {
            dispatch(clearArtistsError());
        }
    }, [dispatch]);

    let ArtistsContainer = (
        <Grid container spacing={2} direction="column" style={{paddingTop: "20px"}}>
            <Grid item>
                <h2 style={{textAlign: "center"}}>Artists</h2>
                <ArtistsList artists={artists}/>
            </Grid>
        </Grid>
    );

    if (loading === true) {
        ArtistsContainer = (
            <div
                style={{
                    position: "relative",
                    paddingLeft: "50%",
                    paddingTop: "30%"
                }}
            >
                <CircularProgress
                    color="secondary"
                    size={100}
                />
            </div>
        );
    }

    return ArtistsContainer;
};

export default HomePage;