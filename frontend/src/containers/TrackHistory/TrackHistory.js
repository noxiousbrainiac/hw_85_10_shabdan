import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getTrackHistory} from "../../store/actions/trackHistoryActions";
import {Redirect} from "react-router-dom";
import HistoryList from "../../components/Lists/HistoryList/HistoryList";

const TrackHistory = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const trackHistory = useSelector(state => state.trackHistory.trackHistory);

    useEffect(() => {
        dispatch(getTrackHistory());
    }, [dispatch]);

    if (!user) {
        return <Redirect to="/login"/>
    }

    return (
        <>
            <h2>Your history</h2>
            <HistoryList trackHistory={trackHistory}/>
        </>
    );
};

export default TrackHistory;