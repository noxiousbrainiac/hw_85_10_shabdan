import axios from "axios";

const AxiosApi = axios.create({
    baseURL: "http://localhost:8000"
});

export default AxiosApi;