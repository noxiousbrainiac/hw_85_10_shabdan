import React from 'react';
import {Grid} from "@material-ui/core";
import AlbumCard from "./AlbumCard/AlbumCard";

const AlbumsList = ({albums}) => {
    return (
        <Grid item container direction="row" spacing={1}>
            {albums.length !== 0 ? albums.map(item => (
                <AlbumCard
                    key={item._id}
                    photo={item.image}
                    title={item.title}
                    year={item.productionYear}
                    id={item._id}
                    published={item.published}
                />
            )) : <h3>No albums here</h3>}
        </Grid>

    );
};

export default AlbumsList;