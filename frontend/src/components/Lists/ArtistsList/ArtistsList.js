import React from 'react';
import ArtistCard from "./ArtistCard/ArtistCard";
import {Grid} from "@material-ui/core";

const ArtistsList = ({artists}) => {
    return (
        <>
            <Grid item container direction="row" spacing={1}>
                {artists.length !== 0 ? artists.map(item => (
                    <ArtistCard
                        key={item._id}
                        name={item.title}
                        photo={item?.image}
                        id={item._id}
                        published={item.published}
                    />
                )) : <h3>No artists here</h3>}
            </Grid>
        </>
    );
};

export default ArtistsList;