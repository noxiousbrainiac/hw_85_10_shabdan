import React from 'react';
import {Grid} from "@material-ui/core";
import TrackItem from "./TrackItem/TrackItem";

const TracksList = ({tracks}) => {
    return (
        <Grid item container direction="row" spacing={2}>
            {tracks.length !== 0  ? tracks.map(item => (
                <TrackItem
                    key={item._id}
                    title={item.title}
                    duration={item.duration}
                    number={item.trackNumber}
                    id={item._id}
                    published={item.published}
                />
            )) : <h3>No tracks here</h3>}
        </Grid>
    );
};

export default TracksList;