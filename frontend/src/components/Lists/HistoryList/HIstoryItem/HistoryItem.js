import React from 'react';
import {Card, CardHeader, Grid, makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    card: {
        height: '100%',
        cursor: "pointer",
        padding: "10px"
    }
});

const HistoryItem = ({artist, track, date, }) => {
    const classes = useStyles();

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card
                className={classes.card}
            >
                <CardHeader title={`Track: ${track}`}/>
                <p>Date: <b>{date}</b></p>
                <p>Artist: <b>{artist}</b></p>
            </Card>
        </Grid>
    );
};

export default HistoryItem;