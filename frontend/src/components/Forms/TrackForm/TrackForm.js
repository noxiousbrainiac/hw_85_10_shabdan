import React, {useState} from 'react';
import {Grid, makeStyles} from "@material-ui/core";
import FormElement from "../../UI/Form/FormElement";
import ButtonWithProgress from "../../UI/ButtonWithProgress/ButtonWithProgress";
import {useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const TrackForm = ({onSubmit, albums, error}) => {
    const classes = useStyles();
    const loading = useSelector(state1 => state1.tracksReducer.loading);

    const [state, setState] = useState({
        title: "",
        album: "",
        duration: "",
        trackNumber: 0
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >

            <FormElement
                select
                options={albums}
                label="Album"
                name="album"
                value={state.album}
                onChange={inputChangeHandler}
                error={getFieldError('album')}
            />

            <FormElement
                label="Title"
                name="title"
                value={state.title}
                onChange={inputChangeHandler}
                error={getFieldError('title')}
            />

            <FormElement
                label="Duration"
                name="duration"
                value={state.duration}
                onChange={inputChangeHandler}
                error={getFieldError('duration')}
            />

            <FormElement
                label="Track number"
                name="trackNumber"
                value={state.trackNumber}
                onChange={inputChangeHandler}
                error={getFieldError('trackNumber')}
            />

            <Grid item xs>
                <ButtonWithProgress
                    type="submit"
                    variant="contained"
                    color="primary"
                    loading={loading}
                    disabled={loading}
                >
                    Create
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default TrackForm;