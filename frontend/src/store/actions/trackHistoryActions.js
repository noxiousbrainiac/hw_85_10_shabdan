import AxiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';

export const GET_TRACK_HISTORY_REQUEST = "GET_TRACK_HISTORY_REQUEST";
export const GET_TRACK_HISTORY_SUCCESS = "GET_TRACK_HISTORY_SUCCESS";
export const GET_TRACK_HISTORY_FAILURE = "GET_TRACK_HISTORY_FAILURE";

export const getTrackHistoryRequest = () => ({type: GET_TRACK_HISTORY_REQUEST});
export const getTrackHistorySuccess = (data) => ({type: GET_TRACK_HISTORY_SUCCESS, payload: data});
export const getTrackHistoryFailure = (error) => ({type: GET_TRACK_HISTORY_FAILURE, payload :error});

export const getTrackHistory = () => async (dispatch) => {
    try {
        dispatch(getTrackHistoryRequest());
        const {data} = await AxiosApi.get('/trackhistories');
        dispatch(getTrackHistorySuccess(data));
        console.log(data);
    } catch (e) {
        dispatch(getTrackHistoryFailure(e));
        if (e.response.status === 401) {
            toast.warning('You need login');
        } else {
            toast.error('Could not fetch history!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
}