import AxiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush, historyReplace} from "./historyActions";
import WarningIcon from "@material-ui/icons/Warning";

export const GET_ALBUMS_REQUEST = "GET_ALBUMS_REQUEST";
export const GET_ALBUMS_SUCCESS = "GET_ALBUMS_SUCCESS";
export const GET_ALBUMS_FAILURE = "GET_ALBUMS_FAILURE";

export const POST_ALBUM_REQUEST = "POST_ALBUM_REQUEST";
export const POST_ALBUM_SUCCESS = "POST_ALBUM_SUCCESS";
export const POST_ALBUM_FAILURE = "POST_ALBUM_FAILURE";

export const CLEAR_ALBUMS_ERROR = "CLEAR_ALBUMS_ERROR";

export const DELETE_ALBUM_REQUEST = "DELETE_ALBUM_REQUEST";
export const DELETE_ALBUM_SUCCESS = "DELETE_ALBUM_SUCCESS";
export const DELETE_ALBUM_FAILURE = "DELETE_ALBUM_FAILURE";

export const PUBLISH_ALBUM_REQUEST = "PUBLISH_ALBUM_REQUEST";
export const PUBLISH_ALBUM_SUCCESS = "PUBLISH_ALBUM_SUCCESS";
export const PUBLISH_ALBUM_FAILURE = "PUBLISH_ALBUM_FAILURE";

export const publishAlbumRequest = () => ({type: PUBLISH_ALBUM_REQUEST});
export const publishAlbumSuccess = (payload) => ({type: PUBLISH_ALBUM_SUCCESS, payload});
export const publishAlbumFailure = (payload) => ({type: PUBLISH_ALBUM_FAILURE, payload});

export const getAlbumsRequest = () => ({type: GET_ALBUMS_REQUEST});
export const getAlbumsSuccess = (albums) => ({type: GET_ALBUMS_SUCCESS, payload: albums});
export const getAlbumsFailure = (error) => ({type: GET_ALBUMS_FAILURE, payload: error});

export const postAlbumRequest = () => ({type: POST_ALBUM_REQUEST});
export const postAlbumSuccess = () => ({type: POST_ALBUM_SUCCESS});
export const postAlbumFailure = (payload) => ({type: POST_ALBUM_FAILURE, payload});

export const clearAlbumsError = () => ({type: CLEAR_ALBUMS_ERROR});

export const deleteAlbumRequest = () => ({type: DELETE_ALBUM_REQUEST});
export const deleteAlbumSuccess = () => ({type: DELETE_ALBUM_SUCCESS});
export const deleteAlbumFailure = (e) => ({type: DELETE_ALBUM_FAILURE, payload: e});

export const getAlbums = (query) => async (dispatch) => {
    try {
        let response;
        dispatch(getAlbumsRequest());
        if (query) {
            response = await AxiosApi.get(`/albums${query}`);
        } else {
            response = await AxiosApi.get(`/albums`);
        }
        dispatch(getAlbumsSuccess(response.data));
    } catch (e) {
        dispatch(getAlbumsFailure(e));
        toast.error("Не получилось получить данные");
    }
}

export const postAlbum = (albumData) => async (dispatch) => {
    try {
        dispatch(postAlbumRequest());
        await AxiosApi.post('/albums', albumData);
        dispatch(postAlbumSuccess());
        dispatch(historyPush('/'));
        toast.success('Album created');
        dispatch(historyReplace('/'));
    } catch (e) {
        if (e.response && e.response.data) {
            dispatch(postAlbumFailure(e.response.data));
        } else {
            dispatch(postAlbumFailure({error: 'Required input'}));
        }
    }
};

export const deleteAlbum = (id, location) => async (dispatch) => {
    try {
        dispatch(deleteAlbumRequest());
        await AxiosApi.delete(`/albums/${id}`);
        dispatch(deleteAlbumSuccess());
        toast.success('Album deleted successful');
        dispatch(getAlbums(location));
    } catch (e) {
        dispatch(deleteAlbumFailure(e));
        if (e.response.status === 404) {
            dispatch(deleteAlbumFailure({message: "Album is not found"}));
            toast.error("Альбом не найден");
        }
    }
};

export const publishAlbum = (id, location) => async (dispatch) => {
    try {
        dispatch(publishAlbumRequest());
        const {data} =  await AxiosApi.post(`/albums/${id}/publish`);
        dispatch(publishAlbumSuccess(data));
        dispatch(getAlbums(location));
        toast.success(`Album's publish status changed`);
    } catch (e) {
        dispatch(publishAlbumFailure(e));
        if (e.response.status === 404) {
            toast.error("Альбом не найден");
        } else if (e.response.status === 403) {
            toast.error("У вса нет прав на редактирование этого альбома!");
        } else {
            toast.error(`Something wrong! ${e.response.data.error}` , {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

