import AxiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';

export const GET_TRACKS_REQUEST = "GET_TRACKS_REQUEST";
export const GET_TRACKS_SUCCESS = "GET_TRACKS_SUCCESS";
export const GET_TRACKS_FAILURE = "GET_TRACKS_FAILURE";

export const ADD_TRACK_REQUEST = "ADD_TRACK_REQUEST";
export const ADD_TRACK_SUCCESS = "ADD_TRACK_SUCCESS";
export const ADD_TRACK_FAILURE = "ADD_TRACK_FAILURE";

export const POST_TRACK_REQUEST = "POST_TRACK_REQUEST";
export const POST_TRACK_SUCCESS = "POST_TRACK_SUCCESS";
export const POST_TRACK_FAILURE = "POST_TRACK_FAILURE";

export const CLEAR_TRACK_ERROR = "CLEAR_TRACK_ERROR";

export const DELETE_TRACK_REQUEST = "DELETE_TRACK_REQUEST";
export const DELETE_TRACK_SUCCESS = "DELETE_TRACK_SUCCESS";
export const DELETE_TRACK_FAILURE = "DELETE_TRACK_FAILURE";

export const PUBLISH_TRACK_REQUEST = "PUBLISH_TRACK_REQUEST";
export const PUBLISH_TRACK_SUCCESS = "PUBLISH_TRACK_SUCCESS";
export const PUBLISH_TRACK_FAILURE = "PUBLISH_TRACK_FAILURE";

export const publishTrackRequest = () => ({type: PUBLISH_TRACK_REQUEST});
export const publishTrackSuccess = () => ({type: PUBLISH_TRACK_SUCCESS});
export const publishTrackFailure = (payload) => ({type: PUBLISH_TRACK_FAILURE, payload});

export const getTracksRequest = () => ({type: GET_TRACKS_REQUEST});
export const getTracksSuccess = (tracks) => ({type: GET_TRACKS_SUCCESS, payload: tracks});
export const getTracksFailure = (error) => ({type: GET_TRACKS_FAILURE, payload: error});

export const addTrackRequest = () => ({type: ADD_TRACK_REQUEST});
export const addTrackSuccess = () => ({type: ADD_TRACK_SUCCESS});
export const addTrackFailure = (error) => ({type: ADD_TRACK_FAILURE, error});

export const postTrackRequest = () => ({type: POST_TRACK_REQUEST});
export const postTrackSuccess = () => ({type: POST_TRACK_SUCCESS});
export const postTrackFailure = (e) => ({type: POST_TRACK_FAILURE, payload: e});

export const clearTrackError = () => ({type: CLEAR_TRACK_ERROR});

export const deleteTrackRequest = () => ({type: DELETE_TRACK_REQUEST});
export const deleteTrackSuccess = () => ({type: DELETE_TRACK_SUCCESS});
export const deleteTrackFailure = (payload) => ({type: DELETE_TRACK_FAILURE, payload});

export const getTracks = (query) => async (dispatch) => {
    try {
        dispatch(getTracksRequest());
        const {data} = await AxiosApi(`/tracks${query}`)
        dispatch(getTracksSuccess(data));
    } catch (e) {
        dispatch(getTracksFailure(e));
    }
}

export const addTrackToHistory = (track_id) => async (dispatch) => {
    try {
        dispatch(addTrackRequest());
        const {data} = await AxiosApi.post('/trackhistories', {track_id});
        dispatch(addTrackSuccess(data));
        dispatch(historyPush('/trackhistory'));
    } catch (e) {
        dispatch(addTrackFailure(e));
        if (e.response.status === 401) {
            toast.warning('You need login');
        }
        else {
            toast.error('Could not fetch history!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

export const postTrack = (trackData) => async (dispatch) => {
  try {
      dispatch(postTrackRequest());
      await AxiosApi.post('/tracks', trackData);
      dispatch(postTrackSuccess());
      dispatch(historyPush('/'));
      toast.success('Track created');
  } catch (e) {
      if (e.response && e.response.data) {
          dispatch(postTrackFailure(e.response.data));
      } else {
          dispatch(postTrackFailure({error: 'Required input'}));
      }
  }
};

export const deleteTrack = (id, location) => async (dispatch) => {
    try {
        dispatch(deleteTrackRequest());
        await AxiosApi.delete(`/tracks/${id}`);
        dispatch(deleteTrackSuccess());
        dispatch(getTracks(location));
        toast.success('Track deleted');
    } catch (e) {
        dispatch(deleteTrackFailure());
        if (e.response.status === 404) {
            toast.error("Трек не найден");
        } else if (e.response.status === 403) {
            toast.error("У вса нет прав на удаление этого трека!");
        } else {
            toast.error(`Something wrong! ${e.response.data}` , {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

export const publishTrack = (id, location) => async (dispatch) => {
    try {
        dispatch(publishTrackRequest());
        await AxiosApi.post(`/tracks/${id}/publish`);
        dispatch(publishTrackSuccess());
        dispatch(getTracks(location));
        toast.success(`Track's publish status changed`);
    } catch (e) {
        dispatch(publishTrackFailure(e));
        if (e.response.status === 404) {
            toast.error("Трек не найден");
        } else if (e.response.status === 403) {
            toast.error("У вса нет прав на редактирование этого трека!");
        } else {
            toast.error(`Something wrong! ${e.response.data.error}` , {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};