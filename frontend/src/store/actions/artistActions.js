import AxiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";
import WarningIcon from '@material-ui/icons/Warning';

export const GET_ARTISTS_REQUEST = "GET_ARTISTS_REQUEST";
export const GET_ARTISTS_SUCCESS = "GET_ARTISTS_SUCCESS";
export const GET_ARTISTS_FAILURE = "GET_ARTISTS_FAILURE";

export const POST_ARTIST_REQUEST = "POST_ARTIST_REQUEST";
export const POST_ARTIST_SUCCESS = "POST_ARTIST_SUCCESS";
export const POST_ARTIST_FAILURE = "POST_ARTIST_FAILURE";

export const CLEAR_ARTISTS_ERROR = "CLEAR_ARTISTS_ERROR";

export const DELETE_ARTIST_REQUEST = "DELETE_ARTIST_REQUEST";
export const DELETE_ARTIST_SUCCESS = "DELETE_ARTIST_SUCCESS";
export const DELETE_ARTIST_FAILURE = "DELETE_ARTIST_FAILURE";

export const PUBLISH_ARTIST_REQUEST = "PUBLISH_ARTIST_REQUEST";
export const PUBLISH_ARTIST_SUCCESS = "PUBLISH_ARTIST_SUCCESS";
export const PUBLISH_ARTIST_FAILURE = "PUBLISH_ARTIST_FAILURE";

export const publishArtistRequest = () => ({type: PUBLISH_ARTIST_REQUEST});
export const publishArtistSuccess = (payload) => ({type: PUBLISH_ARTIST_SUCCESS, payload});
export const publishArtistFailure = (payload) => ({type: PUBLISH_ARTIST_FAILURE, payload});

export const getArtistsRequest = () => ({type: GET_ARTISTS_REQUEST});
export const getArtistsSuccess = (artists) => ({type: GET_ARTISTS_SUCCESS, payload: artists});
export const getArtistsFailure = (error) => ({type: GET_ARTISTS_FAILURE, payload: error});

export const clearArtistsError = () => ({type: CLEAR_ARTISTS_ERROR});

export const postArtistRequest = () => ({type: POST_ARTIST_REQUEST});
export const postArtistSuccess = () => ({type: POST_ARTIST_SUCCESS});
export const postArtistFailure = (payload) => ({type: POST_ARTIST_FAILURE, payload});

export const deleteArtistRequest = () => ({type: DELETE_ARTIST_REQUEST});
export const deleteArtistSuccess = () => ({type: DELETE_ARTIST_SUCCESS});
export const deleteArtistFailure = (e) => ({type: DELETE_ARTIST_FAILURE, payload: e});

export const getArtists = () => async (dispatch) => {
    try {
        dispatch(getArtistsRequest());
        const {data} = await AxiosApi.get('/artists');
        dispatch(getArtistsSuccess(data));
    } catch (e) {
        dispatch(getArtistsFailure(e));
        toast.error("Не получилось получить данные");
    }
};

export const postArtist = (artistData) => async (dispatch) => {
  try {
      dispatch(postArtistRequest());
      await AxiosApi.post('/artists', artistData);
      dispatch(postArtistSuccess());
      dispatch(historyPush('/'));
      toast.success('Artist created');
  }  catch (e) {
      dispatch(postArtistFailure(e));
      if (e.response && e.response.data) {
          dispatch(postArtistFailure(e.response.data));
      } else {
          dispatch(postArtistFailure({error: 'Required input'}));
      }
  }
};

export const deleteArtist = (id) => async (dispatch) => {
    try {
        dispatch(deleteArtistRequest());
        await AxiosApi.delete(`/artists/${id}`);
        dispatch(deleteArtistSuccess());
        dispatch(getArtists());
        toast.success('Artist deleted');
    } catch (e) {
        dispatch(deleteArtistFailure(e));
        if (e.response.status === 404) {
            toast.error("Артист не найден");
        } else if (e.response.status === 403) {
            toast.error("У вса нет прав на удаление этого артиста!");
        } else {
            toast.error(`Something wrong! ${e.response.data}` , {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

export const publishArtist = (id) => async (dispatch) => {
    try {
        dispatch(publishArtistRequest());
        const {data} = await AxiosApi.post(`/artists/${id}/publish`);
        dispatch(publishArtistSuccess(data));
        dispatch(getArtists());
        toast.success(`Artist's publish status changed`);
    } catch (e) {
        dispatch(publishArtistFailure(e));
        if (e.response.status === 404) {
            toast.error("Артист не найден");
        } else if (e.response.status === 403) {
            toast.error("У вса нет прав на редактирование этого артиста!");
        } else {
            toast.error(`Something wrong! ${e.response.data.error}` , {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};