import {
    CLEAR_ARTISTS_ERROR,
    DELETE_ARTIST_FAILURE,
    DELETE_ARTIST_REQUEST,
    DELETE_ARTIST_SUCCESS,
    GET_ARTISTS_FAILURE,
    GET_ARTISTS_REQUEST,
    GET_ARTISTS_SUCCESS,
    POST_ARTIST_FAILURE,
    POST_ARTIST_REQUEST,
    POST_ARTIST_SUCCESS,
    PUBLISH_ARTIST_FAILURE,
    PUBLISH_ARTIST_REQUEST,
    PUBLISH_ARTIST_SUCCESS
} from "../actions/artistActions";

const initialState = {
    artists: [],
    error: null,
    loading: false,
    postError: null,
    artist: {}
}

const artistReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ARTISTS_REQUEST:
            return {...state, loading: true};
        case GET_ARTISTS_SUCCESS:
            return {...state, artists: action.payload, loading: false};
        case GET_ARTISTS_FAILURE:
            return {...state, error: action.payload, loading: false};
        case POST_ARTIST_REQUEST:
            return {...state, loading: true};
        case POST_ARTIST_SUCCESS:
            return {...state, loading: false};
        case POST_ARTIST_FAILURE:
            return {...state, loading: false, postError: action.payload};
        case CLEAR_ARTISTS_ERROR:
            return {...state, error: null, postError: null};
        case DELETE_ARTIST_REQUEST:
            return {...state, loading: true};
        case DELETE_ARTIST_SUCCESS:
            return {...state, loading: false};
        case DELETE_ARTIST_FAILURE:
            return {...state, error: action.payload};
        case PUBLISH_ARTIST_REQUEST:
            return {...state, loading: true};
        case PUBLISH_ARTIST_SUCCESS:
            return {...state, loading: false, artist: action.payload};
        case PUBLISH_ARTIST_FAILURE:
            return {...state, error: action.payload};
        default: return state;
    }
};

export default artistReducer;