import {
    CLEAR_ALBUMS_ERROR, DELETE_ALBUM_FAILURE, DELETE_ALBUM_REQUEST, DELETE_ALBUM_SUCCESS,
    GET_ALBUMS_FAILURE,
    GET_ALBUMS_REQUEST,
    GET_ALBUMS_SUCCESS, POST_ALBUM_FAILURE,
    POST_ALBUM_REQUEST,
    POST_ALBUM_SUCCESS, PUBLISH_ALBUM_FAILURE, PUBLISH_ALBUM_REQUEST, PUBLISH_ALBUM_SUCCESS
} from "../actions/albumsActions";

const initialState = {
    albums: [],
    error: null,
    loading: false,
    postError: null,
    album: {}
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALBUMS_REQUEST:
            return {...state, loading: true};
        case GET_ALBUMS_SUCCESS:
            return {...state, loading: false, albums: action.payload};
        case GET_ALBUMS_FAILURE:
            return {...state, loading: false, error: action.payload};
        case POST_ALBUM_REQUEST:
            return {...state, loading: true};
        case POST_ALBUM_SUCCESS:
            return {...state, loading: false};
        case POST_ALBUM_FAILURE:
            return {...state, loading: false, postError: action.payload};
        case CLEAR_ALBUMS_ERROR:
            return {...state, error: null, postError: null};
        case DELETE_ALBUM_REQUEST:
            return {...state, loading: true};
        case DELETE_ALBUM_SUCCESS:
            return {...state, loading: false};
        case DELETE_ALBUM_FAILURE:
            return {...state, loading: false, error: action.payload};
        case PUBLISH_ALBUM_REQUEST:
            return {...state, loading: true};
        case PUBLISH_ALBUM_SUCCESS:
            return {...state, loading: false, album: action.payload};
        case PUBLISH_ALBUM_FAILURE:
            return {...state, loading: false, error: action.payload};
        default: return state;
    }
};

export default albumsReducer;