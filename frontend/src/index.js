import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Router} from "react-router-dom";
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import artistReducer from "./store/reducers/artistReducer";
import albumsReducer from "./store/reducers/albumsReducer";
import tracksReducer from "./store/reducers/tracksReducer";
import {createTheme, MuiThemeProvider} from "@material-ui/core";
import userReducer from "./store/reducers/userReducer";
import history from "./history";
import trackHistoryReducer from "./store/reducers/trackHistoryReducer";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {loadFromLocalStorage, saveToLocalStorage} from "./store/localStorage";
import AxiosApi from "./axiosApi";

const rootReducer = combineReducers({
    artistReducer: artistReducer,
    albumsReducer: albumsReducer,
    tracksReducer: tracksReducer,
    users: userReducer,
    trackHistory: trackHistoryReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunk)));

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        },
    });
});

const theme = createTheme({
    props: {
        MuiTextField: {
            variant: "outlined",
            fullWidth: true
        }
    }
});

AxiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token
    } catch (e) {}

    return config;
});

const app = (
        <Provider store={store}>
            <Router history={history}>
                <MuiThemeProvider theme={theme}>
                    <ToastContainer/>
                    <App/>
                </MuiThemeProvider>
            </Router>
        </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
