const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TracksSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Albums',
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    trackNumber: {
        type: Number,
        required: true
    },
    published: {
        type: Boolean,
        required: true,
        enum: [true, false],
        default: false
    },
});

TracksSchema.plugin(idValidator);
const Tracks = mongoose.model('Tracks', TracksSchema);
module.exports = Tracks;