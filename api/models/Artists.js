const mongoose = require('mongoose');

const ArtistsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    image: String,
    info: String,
    published: {
        type: Boolean,
        required: true,
        enum: [true, false],
        default: false
    },
});

const Artists = mongoose.model('Artists', ArtistsSchema)
module.exports = Artists;