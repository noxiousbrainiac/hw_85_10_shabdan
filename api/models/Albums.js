const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const AlbumsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artists',
        required: true
    },
    productionYear: {
        type: String,
        required: true,
    },
    image: String,
    published: {
        type: Boolean,
        required: true,
        enum: [true, false],
        default: false
    },
});

AlbumsSchema.plugin(idValidator);
const Albums = mongoose.model('Albums', AlbumsSchema);
module.exports = Albums;