const express = require('express');
const Tracks = require('../models/Tracks');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const User = require("../models/User");
const {ValidationError} = require('mongoose').Error;

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const token = req.get('Authorization');

        if (!token) {
            const track = await Tracks.find({album: req.query.album, published: true})
                .populate({path: 'album', select: 'title', populate: {path: 'artist', select: 'title'}})
                .sort({trackNumber: +1});
            return res.send(track);
        }

        const user = await User.findOne({token});

        if (user.role !== 'admin') {
            if (req.query.album) {
                const track = await Tracks.find({album: req.query.album, published: true})
                    .populate({path: 'album', select: 'title', populate: {path: 'artist', select: 'title'}})
                    .sort({trackNumber: +1});
                return res.send(track);
            }

            const tracks = await Tracks.find();
            return res.send(tracks);
        }

        if (req.query.album) {
            const track = await Tracks.find({album: req.query.album})
                .populate({path: 'album', select: 'title', populate: {path: 'artist', select: 'title'}})
                .sort({trackNumber: +1});
            return res.send(track);
        }

        const tracks = await Tracks.find();
        return res.send(tracks);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
   try {
       const track = await Tracks.findById(req.params.id);

       if (track) {
           return res.send(track);
       } else {
           return res.status(404).send({error: "Track not found"});
       }
   } catch (e) {
       res.status(500).send(e);
   }
});

router.post('/', auth , permit('admin') ,async (req, res) => {
    try {
        const trackData = {
            title: req.body.title,
            album: req.body.album,
            duration: req.body.duration,
            trackNumber: req.body.trackNumber
        }

        const track = new Tracks(trackData);
        await track.save();
        res.send(track);
    } catch (e) {
        if(e instanceof ValidationError) {
            return res.status(400).send(e);
        }
        res.status(500).send(e);
    }
});

router.post('/:id/publish', auth, permit('admin'),async (req, res) => {
    try {
        const tracks = await Tracks.findById(req.params.id);

        if (tracks) {
            tracks.published = !tracks.published;
            await tracks.save();
            res.send(tracks);
        } else {
            res.status(404).send({message: 'Not found'});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id',  auth, permit('admin'),async(req, res) => {
    try {
        const artist = await Tracks.findByIdAndDelete(req.params.id);

        if (artist) {
            res.send(`Track by ${req.params.id} was deleted successful`);
        } else {
            res.status(404).send({message: 'Track not found'});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;