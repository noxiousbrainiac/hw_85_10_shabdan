const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const Tracks = require('../models/Tracks');
const mongoose = require('mongoose');
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth,async (req, res) => {
    try {
        if (!req.body.track_id) return res.status(401).send({error: "No track id"});

        const track = await Tracks.findOne({_id: req.body.track_id});

        if (!track) return res.status(404).send({error: "No track with following ID"});

        const historyData = {
            track: track._id,
            user: req.user._id,
            datetime: Date.now()
        }

        const trackHistory = new TrackHistory(historyData);
        await trackHistory.save();
        res.send(trackHistory);
    } catch (e) {
        if(e instanceof mongoose.CastError) {
           return res.status(400).send({error: "Wrong track's id"});
        }
        res.status(500).send(e);
    }
});

router.get('/', auth, async (req, res) => {
    try {
        const data = await TrackHistory
            .find({user: req.user._id})
            .populate({
                path: 'track',
                select: 'title',
                populate: {path: 'album', select: 'title', populate: {path: 'artist', select: 'name'}}
            }).sort({'datetime': -1});
        res.send(data);
    } catch (e) {
        res.status(500).send(e);
    }
})

module.exports = router;