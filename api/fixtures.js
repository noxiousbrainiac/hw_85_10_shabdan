const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Artists = require('./models/Artists');
const Albums = require('./models/Albums');
const Tracks = require('./models/Tracks');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [admin, user] = await User.create({
        username: 'admin',
        password: '123',
        token: nanoid(),
        role: 'admin'
    }, {
        username: 'user',
        password: '123',
        token: nanoid(),
        role: 'user'
    });

    const [john, bob, lina] = await Artists.create({
        title: 'john',
        image: 'fixtures/john.jpg',
        info: 'from Australia',
        published: false
    }, {
        title: 'bob',
        image: 'fixtures/bob.jpg',
        info: 'from France',
        published: false
    }, {
        title: 'lina',
        image: 'fixtures/lina.jpg',
        info: 'from Japan',
        published: false
    });

    const [goodbye, slowed, crazy] = await Albums.create({
        title: 'Goodbye my life',
        artist: john,
        productionYear: "2015",
        image: 'fixtures/1.jpeg',
        published: false
    }, {
        title: 'Slowed',
        artist: bob,
        productionYear: "2015",
        image: 'fixtures/2.jpeg',
        published: false
    }, {
        title: 'Crazy',
        artist: lina,
        productionYear: "2015",
        image: 'fixtures/3.jpeg',
        published: false
    });

    await Tracks.create({
        title: "Goodbye my lover",
        album: goodbye,
        duration: "2:30",
        trackNumber: 1,
        published: false
    }, {
        title: "Hello world",
        album: goodbye,
        duration: "2:10",
        trackNumber: 2,
        published: false
    }, {
        title: "Gotta",
        album: goodbye,
        duration: "2:35",
        trackNumber: 3,
        published: false
    }, {
        title: "God's plan",
        album: goodbye,
        duration: "2:21",
        trackNumber: 4,
        published: false
    }, {
        title: "Game over",
        album: goodbye,
        duration: "2:51",
        trackNumber: 5,
        published: false
    }, {
        title: "Good day",
        album: slowed,
        duration: "3:30",
        trackNumber: 6,
        published: false
    }, {
        title: "Forbidden voice",
        album: slowed,
        duration: "3:10",
        trackNumber: 7,
        published: false
    }, {
        title: "la-la-la",
        album: slowed,
        duration: "3:35",
        trackNumber: 8,
        published: false
    }, {
        title: "It's a trap",
        album: slowed,
        duration: "3:21",
        trackNumber: 9,
        published: false
    }, {
        title: "Woah!",
        album: slowed,
        duration: "3:51",
        trackNumber: 10,
        published: false
    }, {
        title: "Okolo-koko",
        album: crazy,
        duration: "5:30",
        trackNumber: 11,
        published: false
    }, {
        title: "Spirits",
        album: crazy,
        duration: "5:10",
        trackNumber: 12,
        published: false
    }, {
        title: "Salvador",
        album: crazy,
        duration: "5:35",
        trackNumber: 13,
        published: false
    }, {
        title: "Down-breaker",
        album: crazy,
        duration: "5:21",
        trackNumber: 14,
        published: false
    }, {
        title: "Yoohoo!",
        album: crazy,
        duration: "5:51",
        trackNumber: 15,
        published: false
    });


    await mongoose.connection.close();
};

run().catch(console.error);